# Martha

Make sure your database user has all the necessary permissions:
```
CREATE OR REPLACE DATABASE {YOUR_DB}; GRANT ALL ON *.* TO '{YOUR_USER}'@'localhost' IDENTIFIED BY '{YOUR_PASSWORD}' WITH GRANT OPTION;
```

Also, to enable external login, mariadb server must listen on external address

```
vim /etc/mysql/mariadb.conf.d/50-server.cnf

# Set correct bind-address
# Then restart service: systemctl restart mariadb
```

Install some mariadb plugins to force strong passwords

```bash
apt-get install mariadb-plugin-cracklib-password-check
```

```bash
# in sql
INSTALL SONAME 'simple_password_check';
INSTALL SONAME 'cracklib_password_check';
```