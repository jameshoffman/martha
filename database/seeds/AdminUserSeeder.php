<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'display_name' => env('MARTHA_ADMIN_DISPLAY_NAME'),
            'username' => env('MARTHA_ADMIN_USERNAME'),
            'password' => Hash::make(env('MARTHA_ADMIN_PASSWORD')),
            'database_name' => env('DB_DATABASE'),
            'database_username' => env('DB_USERNAME'),
            'database_password' => env('DB_PASSWORD'),
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),
            'is_admin' => true,
        ]);
    }
}