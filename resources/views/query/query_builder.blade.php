@extends('layouts.master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Query Builder</h1>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Query</h3>
                    </div>

                    <div class="panel-body">
                        @include('errors.list')

                        @include('layouts.flash')

                        {!! Form::open(['action' => 'Queries\QueryBuilderController@execute']) !!}

                        <div class="row">
                            <div class="col-xs-12 form-group">
                                @include('utils.sql_editor', ['text_area_name' => 'query', 'text_area_id' => 'query_textarea'])
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-xs-12 {{ $errors->has('parameters') ? ' has-error' : '' }}">
                                {!! Form::textarea('parameters', null, ['class'=> 'form-control',
                                                                        'placeholder' => 'Parameters JSON',
                                                                        'rows' => '2',
                                                                        'style' => 'max-width: 100%;']) !!}
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-xs-6">
                                {!! Form::submit('Execute statement', ['name' => 'statement', 'class' =>'btn btn-info btn-block']) !!}
                            </div>

                            <div class="col-xs-6">
                                {!! Form::submit('Execute select', ['name' => 'select', 'class' =>'btn btn-success btn-block']) !!}
                            </div>
                        </div>

                        <hr />

                        <h4>Save query</h4>

                        <div class="row form-group">
                            <div class="col-xs-12 {{ $errors->has('name') ? ' has-error' : '' }}">
                                {!! Form::text('name', null, [  'class'=> 'form-control',
                                                                'placeholder' => 'Query name']) !!}
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-xs-6">
                                {!! Form::submit('Add statement', ['name' => 'statement-add', 'class' =>'btn btn-info btn-block']) !!}
                            </div>

                            <div class="col-xs-6">
                                {!! Form::submit('Add select', ['name' => 'select-add', 'class' =>'btn btn-success btn-block']) !!}
                            </div>
                        </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Result</h3>
                    </div>
                    <div class="panel-body">
                        @include('query.result', ['queryRunResponse' => session('queryRunResponse')])
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
