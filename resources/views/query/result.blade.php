<!-- Data to pass: ['queryRunResponse' => ] -->
@if(isset($queryRunResponse))
    <div>
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#table-view" aria-controls="table-view" role="tab" data-toggle="tab">Data</a></li>
            <li role="presentation"><a href="#json-view" aria-controls="json-view" role="tab" data-toggle="tab">JSON</a></li>
            <li role="presentation"><a href="#parameterized-view" aria-controls="parameterized-view" role="tab" data-toggle="tab">Parameterized</a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="table-view">

                @if(empty($queryRunResponse->result['data']))
                    @if($queryRunResponse->result['success'])
                        <h4 style="color: green">Success</h4>
                    @else
                        <h4 style="color: red">Failure</h4>
                        <p>{!! nl2br($queryRunResponse->result['error']) !!}</p>
                    @endif
                @else
                    {!! \App\Services\UIHelperService::dataArrayToTable($queryRunResponse->result['data']) !!}
                @endif
            </div>

            <div role="tabpanel" class="tab-pane" id="json-view">
                @include('utils.json_viewer', ['json' => json_encode($queryRunResponse->result), 'empty_json_message' => 'Nothing to show...', 'id' => 'result'])
            </div>

            <div role="tabpanel" class="tab-pane" id="parameterized-view">
                <h4>Query</h4>
                <p class="sql-code-preview" style="font-size:125%;">{!! nl2br($queryRunResponse->parameterizedQuery) !!}</p>

                <h4 style="">Parameters</h4>
                <div>
                    @include('utils.json_viewer', ['json' => $queryRunResponse->parameters, 'empty_json_message' => 'No parameters...', 'id' => 'params'])
                </div>
            </div>
        </div>
    </div>
@else
    <p>Nothing to show...</p>
@endif