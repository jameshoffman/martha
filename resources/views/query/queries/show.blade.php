@extends('layouts.master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Query</h1>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title pull-left"> {!! $query->name !!} </h3>

                        {{ Form::open(['url' => action('Queries\QueriesController@destroy', ['id' => $query->id])]) }}
                        {{ Form::hidden('_method', 'DELETE') }}
                        {{ Form::submit('Delete', array('class' => 'btn btn-danger btn-xs pull-right')) }}
                        {{ Form::close() }}
                        <div class="clearfix"></div>
                    </div>

                    <div class="panel-body">
                        @include('errors.list')

                        @include('layouts.flash')

                        {!! Form::open(['action' => ['Queries\QueriesController@update', $query->id], 'method'=> 'put']) !!}

                        <div class="row">
                            <div class="col-md-12 form-group">
                                @include('utils.sql_editor', ['text_area_name' => 'query',
                                                              'text_area_id' => 'query_textarea',
                                                              'initial_value' => json_encode(session('oldQuery') == null ? $query->string : session('oldQuery'))])
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-8 col-sm-10 form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                {!! Form::text('name', $query->name, [  'class'=> 'form-control',
                                                                'placeholder' => 'Query name',
                                                                'required' => 'required']) !!}
                            </div>

                            <div class="col-xs-4 col-sm-2 form-group">
                                {!! Form::select('type', ['select' => 'Select', 'statement' => 'Statement'], $query->typeName, [  'class'=> 'form-control']) !!}
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-6 col-sm-4 col-sm-offset-4 col-md-2 col-md-offset-8">
                                {{ link_to_action(  'Queries\QueriesController@show', $title = 'Cancel', [$query->id],
                                                    $attributes = ['class' => 'btn btn-default btn-block']) }}
                            </div>

                            <div class="col-xs-6 col-sm-4 col-md-2">
                                {!! Form::submit('Save', ['class' =>'btn btn-primary btn-block']) !!}
                            </div>
                            <!-- Form is closed below run button-->
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Run query</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row form-group">
                            <div class="col-xs-9 col-md-10 {{ $errors->has('parameters') ? ' has-error' : '' }}">
                                {!! Form::textarea('parameters', null, ['class'=> 'form-control',
                                                                    'placeholder' => 'Parameters JSON',
                                                                    'rows' => '1',
                                                                    'style' => 'max-width: 100%;']) !!}
                            </div>

                            <div class="col-xs-3 col-md-2">
                                {!! Form::submit('Run', ['name' => 'run', 'class' =>'btn btn-default btn-block']) !!}
                            </div>

                            {!! Form::close() !!}
                        </div>

                        @include('query.result', ['queryRunResponse' => session('queryRunResponse')])
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
