@extends('layouts.master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Queries</h1>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">New query</h3>
                    </div>
                    <div class="panel-body">
                        @include('errors.list')

                        @include('layouts.flash')

                        {!! Form::open(['action' => 'Queries\QueriesController@store']) !!}

                        <div class="row">
                            <div class="col-md-12 form-group{{ $errors->has('query') ? ' has-error' : '' }}">
                                @include('utils.sql_editor', ['text_area_name' => 'query', 'text_area_id' => 'query_textarea'])
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-8 col-sm-10 form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                {!! Form::text('name', null, [  'class'=> 'form-control',
                                                                'placeholder' => 'Query name',
                                                                'required' => 'required']) !!}
                            </div>

                            <div class="col-xs-4 col-sm-2 form-group">
                                {!! Form::select('type', ['select' => 'Select', 'statement' => 'Statement'], 'select', [  'class'=> 'form-control']) !!}
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-6 col-sm-4 col-sm-offset-4  col-md-2 col-md-offset-8">
                                {{ link_to_action(  'Queries\QueriesController@index', $title = 'Cancel', $parameters = [],
                                                    $attributes = ['class' => 'btn btn-default btn-block']) }}
                            </div>

                            <div class="col-xs-6 col-sm-4 col-md-2">
                                {!! Form::submit('Add', ['class' =>'btn btn-primary btn-block']) !!}
                            </div>
                        </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title pull-left">List</h3>

                        <div class="pull-right">
                            {{ Form::open(['url' => action('Queries\QueriesController@export'), 'style' => 'margin-bottom: 0;']) }}
                            {{ Form::hidden('_method', 'POST') }}
                            <button type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target=".import-modal">Import</button>
                        {{ Form::submit('Export', array('class' => 'btn btn-info btn-xs')) }}
                        {{ Form::close() }}

                        <!-- Import modal -->
                            <div class="modal fade import-modal" tabindex="-1" role="dialog" aria-labelledby="importQueriesModal">
                                <div class="modal-dialog modal-sm" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Import queries</h4>
                                        </div>

                                        {{ Form::open(['url' => action('Queries\QueriesController@import'), 'files' => true, 'style' => 'margin-bottom: 0;']) }}
                                        {{ Form::hidden('_method', 'POST') }}
                                        <div class="modal-body">
                                            {{ Form::file('queries_file') }}
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                            {{ Form::submit('Import', array('class' => 'btn btn-success')) }}
                                        </div>
                                        {{ Form::close() }}
                                    </div>
                                </div>
                            </div>
                            <!-- -->
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        {{ Form::open(['method' => 'GET', 'url' => action('Queries\QueriesController@index'), 'role' => 'search']) }}
                        <div class="input-group">
                            @if($search)
                                <span class="input-group-btn">
                                    <a href="{{ action("Queries\QueriesController@index") }}" class="btn btn-danger" role="button">Clear</a>
                                </span>
                            @endif

                            {!! Form::text('search', $search, [ 'class'=> 'form-control',
                                                                'placeholder' => 'Search...']) !!}
                            <span class="input-group-btn">
                                {{ Form::submit('Search!', array('class' => 'btn btn-default', 'role' => 'button')) }}
                            </span>
                        </div>
                        {{ Form::close() }}
                    </div>

                    @if($queries->isEmpty())
                        <h3 class="text-center">No queries...</h3>
                    @else
                    <!-- Table -->
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th class="col-xs-2 col-md-2">Type</th>
                                    <th class="col-xs-3 col-md-3">Name</th>
                                    <th class="col-xs-6 col-md-6">Query</th>
                                    <th class="col-xs-1 col-md-1">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($queries as $key => $query)
                                    <tr>
                                        <td><b>{{ strtoupper($query->typeName) }}</b></td>
                                        <td>{{ substr($query->name, 0, 40) }}</td>
                                        <td><i>{{ substr($query->string, 0, 150) }}</i></td>

                                        <td>
                                            {!! link_to_action( 'Queries\QueriesController@show', 'View', [$query->id],
                                                                ['class' => 'btn btn-small btn-warning']) !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
