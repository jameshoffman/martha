<!-- ['table' => ] -->

<div class="panel-group" role="tablist" aria-multiselectable="true">
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="heading{{$table['name']}}">
            <h4 class="panel-title">
                <a class="collapsible-heading collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-{{$table['name']}}" aria-expanded="true" aria-controls="collapse-{{$table['name']}}">
                    {{$table['name']}} <i class="chevron fa fa-fw" ></i>
                </a>
            </h4>
        </div>
        <div id="collapse-{{$table['name']}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-{{$table['name']}}">
            <div class="panel-body">
                <div>
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#table-structure-{{$table['name']}}" aria-controls="table-structure-{{$table['name']}}" role="tab" data-toggle="tab">Structure</a></li>
                        <li role="presentation"><a href="#table-data-{{$table['name']}}" aria-controls="table-data-{{$table['name']}}" role="tab" data-toggle="tab">Data</a></li>
                        <li role="presentation"><a href="#table-create-{{$table['name']}}" aria-controls="table-create-{{$table['name']}}" role="tab" data-toggle="tab">Create Table statement</a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="table-structure-{{$table['name']}}">
                            <h4>Columns</h4>
                            {!! $table['structure']['columns'] !!}

                            <h4>Constraints</h4>
                            @if($table['structure']['constraints'])
                                {!! $table['structure']['constraints'] !!}
                            @else
                                <p>No constraints</p>
                            @endif
                        </div>
                        <div role="tabpanel" class="tab-pane" id="table-data-{{$table['name']}}">
                            @if($table['data'])
                                <h4 class="sql-code-preview">SELECT * FROM {{ $table['name'] }}</h4>
                                {!! $table['data'] !!}
                            @else
                                <br />
                                <p>No data</p>
                            @endif
                        </div>
                        <div role="tabpanel" class="tab-pane" id="table-create-{{$table['name']}}">
                            <p class="sql-code-preview" style="padding-top: 10px;">{!! nl2br($table['create_table']) !!}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>