@extends('layouts.master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="pull-left">Tables</h1>

                <div class="pull-right" style="margin-top: 25px">
                    {{ Form::open(['url' => action('TablesController@mySqlDump')]) }}
                    {{ Form::hidden('_method', 'POST') }}
                    <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target=".import-modal">Import MySQL Dump</button>
                    {{ Form::submit('Export MySQL Dump', array('class' => 'btn btn-info btn-sm')) }}
                    {{ Form::close() }}
                </div>
                <div class="clearfix"></div>

                <!-- Import modal -->
                <div class="modal fade import-modal" tabindex="-1" role="dialog" aria-labelledby="importMySqlDumpModal">
                    <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Import MySQL Dump</h4>
                            </div>

                            {{ Form::open(['url' => action('TablesController@importMySqlDump'), 'files' => true, 'style' => 'margin-bottom: 0;']) }}
                            {{ Form::hidden('_method', 'POST') }}
                            <div class="modal-body">
                                {{ Form::file('mysqldump_file') }}
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                {{ Form::submit('Import', array('class' => 'btn btn-success')) }}
                            </div>
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
                <!-- -->

                @include('errors.list')

                @include('layouts.flash')

                <hr style="margin-top: 0px"/>

                <div>
                    {{ Form::open(['method' => 'GET', 'url' => action('TablesController@index'), 'role' => 'search']) }}
                    <div class="input-group">
                        @if($search)
                            <span class="input-group-btn">
                            <a href="{{ action("TablesController@index") }}" class="btn btn-danger" role="button">Clear</a>
                        </span>
                        @endif

                        {!! Form::text('search', $search, [ 'class'=> 'form-control',
                                                            'placeholder' => 'Search...']) !!}
                        <span class="input-group-btn">
                        {{ Form::submit('Search!', array('class' => 'btn btn-default', 'role' => 'button')) }}
                    </span>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>

        <div class="row" style="padding-top: 20px">
            <div class="col-md-12">
                @if(empty($tables))
                    <h4>No tables to show...</h4>
                @else
                    @foreach($tables as $key => $table)
                        @include('tables.index_row_table', compact('table'))
                    @endforeach
                @endif
            </div>
        </div>
    </div>
@endsection