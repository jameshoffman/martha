@extends('layouts.master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Changelog</h1>
                <a href="https://bitbucket.org/jameshoffman/martha">Source code</a>
                <br />

                <h3>1.4.1</h3>
                <ul>
                    <li>Fixed executing multiples SQL statements in a single query, errors where not correctly returned if at least one statement succeeded.</li>
                </ul>
                <hr />

                <h3>1.4.0</h3>
                <ul>
                    <li>Handled user details.</li>
                    <li>UI tweaks.</li>
                </ul>
                <hr />

                <h3>1.3.12</h3>
                <ul>
                    <li>Fixed api requests logging, it tried to insert even when using invalid username.</li>
                    <li>UI tweak to API Requests, do not show parameterized query if it's not available.</li>
                </ul>
                <hr />

                <h3>1.3.10</h3>
                <ul>
                    <li>Added parameterized query string viewer in query results and API requests.</li>
                    <li>Added redirect to login when Session expires.</li>
                    <li>Added support for newline when displaying queries string.</li>
                </ul>
                <hr />

                <h3>1.3.7</h3>
                <ul>
                    <li>Added MySQL Dump import.</li>
                    <li>Removed <i>is_transaction</i> because queries now supports native SQL transactions.</li>
                </ul>
                <hr />

                <h3>1.3.5</h3>
                <ul>
                    <li>Added queries import/export.</li>
                    <li>Allow to resize parameters input fields.</li>
                </ul>
                <hr />

                <h3>1.3.3</h3>
                <ul>
                    <li>Allow to resize query input areas.</li>
                </ul>
                <hr />

                <h3>1.3.2</h3>
                <ul>
                    <li>Limit records count per user in api_requests table.</li>
                </ul>
                <hr />

                <h3>1.3.1</h3>
                <ul>
                    <li>Added feature to collapse/expand long text in data table viewer.</li>
                </ul>
                <hr />

                <h3>1.3.0</h3>
                <ul>
                    <li>Added <i>Request</i> module to view REST requests received by Martha.</li>
                </ul>
                <hr />

                <h3>1.2.7</h3>
                <ul>
                    <li>Removed accordion behavior from <i>Tables</i>.</li>
                </ul>
                <hr />

                <h3>1.2.6</h3>
                <ul>
                    <li>Fixed users' database default charset to UTF8.</li>
                </ul>
                <hr />

                <h3>1.2.5</h3>
                <ul>
                    <li>UI tweaks.</li>
                    <li>Added search bar in <i>Users</i> admin.</li>
                    <li>Defined multiple levels of exception for users creation/deletion.</li>
                </ul>
                <hr />

                <h3>1.2.2</h3>
                <ul>
                    <li>Added search bar in <i>Tables</i>.</li>
                </ul>
                <hr />

                <h3>1.2.1</h3>
                <ul>
                    <li>Fixed mysqldump path to support development environment and server.</li>
                </ul>
                <hr />

                <h3>1.2.0</h3>
                <ul>
                    <li>Added <i>Tables</i> module to view all tables' structure, data and create table statement. Also allows to generate MySQL dump files.</li>
                    <li>UI tweaks.</li>
                </ul>
                <hr />

                <h3>1.1.0</h3>
                <ul>
                    <li>Added table viewing mode for requests result.</li>
                    <li>Implemented search in queries list.</li>
                </ul>
                <hr />

                <h3>1.0.9</h3>
                <ul>
                    <li>Fixed glitch when parsing request's parameters from the body in REST endpoint.</li>
                </ul>
                <hr />

                <h3>1.0.7</h3>
                <ul>
                    <li>Navigation bar UI tweaks.</li>
                    <li>Added code highlight in new query and query detail.</li>
                    <li>Fixed query detail run bug. Query type was not taken into account.</li>
                    <li>Workaround to return lastInsertId.</li>
                </ul>
                <hr />

                <h3>1.0.3</h3>
                <ul>
                    <li>Fixed a bug when REST endpoint receives a malformed authentication header.</li>
                    <li>Added <i>is transaction</i> attribute to query.</li>
                </ul>
                <hr />

                <h3>1.0.1</h3>
                <ul>
                    <li>UI tweaks.</li>
                    <li>Support multiple SQL statement in a single query.</li>
                </ul>
                <hr />

                <h3>1.0.0</h3>
                <ul>
                    <li>Initial version.</li>
                    <li>Core features to build, execute and save queries.</li>
                    <li>REST endpoint to execute a specific request by its name.</li>
                </ul>
                <hr />
            </div>
        </div>
    </div>
@endsection