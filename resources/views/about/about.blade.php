@extends('layouts.master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>About</h1>

                <p> Martha provides a web interface to execute SQL queries on a database.
                    It also allows to save queries for later use and request them throught a simple REST API.</p>

                <h4>Version 1.4.1</h4>
                {{ link_to_action('AboutController@changelog', 'Changelog') }}

                <hr />

                <h4>Source code</h4>
                <p>Martha source code is available on <a href="https://bitbucket.org/jameshoffman/martha">Bitbucket</a>, feel free to use, share and modify.</p>
                <br/>

                <h4>External dependencies</h4>
                <p>Martha is built with the PHP framework <a href="http://laravel.com">Laravel</a> and uses the following librairies:</p>

                <dl>
                    <dt><a href="https://github.com/lmenezes/json-tree">json-tree</a></dt>
                    <dd>JavaScript library which generates navigable HTML tree structure from a JSON string.</dd>
                    <br/>

                    <dt><a href="http://codemirror.net/">CodeMirror</a></dt>
                    <dd>JavaScript text editor supporting advanced editing features and syntax highlighting for many languages.</dd>
                </dl>
            </div>
        </div>
    </div>
@endsection