<!--
    ['text_area_name' => '', 'text_area_id' => '', 'initial_value' => '', 'text_area_min_height' => '']

    optionals: initial_value, text_area_min_height
-->
{!! Form::textarea($text_area_name, '',['id' => $text_area_id,
                                        'class' => 'form-control', 'style' => 'max-width: 100%;']) !!}
<div class="query-handle" id="query_handle_{{$text_area_id}}"></div>
<script>
    const MIN_HEIGHT_{{$text_area_id}} = {{ isset($text_area_min_height) ? $text_area_min_height : 150 }};

    var codeMirror_{{$text_area_id}};
    var handle_{{$text_area_id}};
    var currentSize_{{$text_area_id}} = MIN_HEIGHT_{{$text_area_id}};

    var start_x_{{$text_area_id}};
    var start_y_{{$text_area_id}};
    var start_h_{{$text_area_id}};

    var queryTextArea = document.getElementById('{{$text_area_id}}');

    codeMirror_{{$text_area_id}} = CodeMirror.fromTextArea(queryTextArea, {
        mode: "text/x-mysql",
        lineNumbers: true,
        autoRefresh: true,
        indentWithTabs: true,
        smartIndent: true }
    );
    codeMirror_{{$text_area_id}}.setSize(null, currentSize_{{$text_area_id}}+"px");

    @if(isset($initial_value))
        codeMirror_{{$text_area_id}}.getDoc().setValue({!! $initial_value !!});
    @endif

    handle_{{$text_area_id}} = document.querySelector("#query_handle_{{$text_area_id}}");

    handle_{{$text_area_id}}.addEventListener("mousedown", function (e) {
        start_x_{{$text_area_id}} = e.x;
        start_y_{{$text_area_id}} = e.y;
        start_h_{{$text_area_id}} = currentSize_{{$text_area_id}};

        document.body.addEventListener("mousemove", on_drag_{{$text_area_id}});
        window.addEventListener("mouseup", on_release_{{$text_area_id}});
    });

    function on_drag_{{$text_area_id}}(e) {
        currentSize_{{$text_area_id}} = Math.max(MIN_HEIGHT_{{$text_area_id}}, (start_h_{{$text_area_id}} + e.y - start_y_{{$text_area_id}}))
        codeMirror_{{$text_area_id}}.setSize(null, currentSize_{{$text_area_id}} + "px");
    }

    function on_release_{{$text_area_id}}(e) {
        document.body.removeEventListener("mousemove", on_drag_{{$text_area_id}});
        window.removeEventListener("mouseup", on_release_{{$text_area_id}});
    }
</script>