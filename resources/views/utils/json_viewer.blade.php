<!-- ['json' => , 'empty_json_message' => '', 'id' => '']
json is a string, valid JSON or not
-->
<div id="result-result-field-{{$id}}" data-result-field="{{ $json }}" ></div>
<div id="result-json-viewer-{{$id}}"></div>
<script>
    var resultDataJson_{{$id}} = $('#result-result-field-{{$id}}').data("result-field");
    if (resultDataJson_{{$id}}) {
        var jsonTreeResult_{{$id}} = JSONTree.create(resultDataJson_{{$id}});
        var failedParseJsonTree_{{$id}}= "<span class=\"jstValue\"><span class=\"jstStr\">\"{{ $json }}\"</span></span>";

        if (jsonTreeResult_{{$id}} == failedParseJsonTree_{{$id}}) {
            document.getElementById("result-json-viewer-{{$id}}").innerHTML = "<p style=\"color: red;\">Invalid JSON format</p><p>" + resultDataJson_{{$id}} + "</p>";
        } else {
            document.getElementById("result-json-viewer-{{$id}}").innerHTML = jsonTreeResult_{{$id}};
        }
    } else {
        document.getElementById("result-json-viewer-{{$id}}").innerHTML = "<p>{{$empty_json_message}}<p>";
    }
</script>
