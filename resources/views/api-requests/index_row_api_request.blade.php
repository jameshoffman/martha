<!-- Data to pass: ['request' => ] -->
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">{{ $request["created_at"] }}, <i>{{ $request["query_name"] }}</i></h3>
    </div>
    <div class="panel-body">
        <p class="sql-code-preview" style="font-size:125%;">{!! nl2br($request["query_string"]) !!}</p>

        <!-- PARAMETERS -->
        <h4 style="">Parameters
            @if($request["body_parameters"])
                <i><a style="font-size: 75%;" id="details-toggle-body-parameters-{{$request['id']}}" data-toggle="collapse" href="#collapse-body-parameters-{{$request['id']}}" aria-expanded="false" aria-controls="collapse-body-parameters-{{$request['id']}}">
                        Hide
                    </a></i>
            @endif
        </h4>
        @if($request["body_parameters"])
            <div class="collapse in" id="collapse-body-parameters-{{$request['id']}}">
                @include('utils.json_viewer', ['json' => $request["body_parameters"],
                                               'empty_json_message' => 'No parameters...',
                                               'id' => $request["id"]])

                @if($request["query_parameterized_string"])
                    <h5><i>Parameterized query</i></h5>
                    <p class="sql-code-preview" style="margin-bottom: 20px;">{!! nl2br($request['query_parameterized_string']) !!}</p>
                @endif
            </div>

            <script>
                $('#collapse-body-parameters-{{$request['id']}}').on('hide.bs.collapse', function () {
                    document.getElementById('details-toggle-body-parameters-{{$request['id']}}').innerHTML = "Show"
                })

                $('#collapse-body-parameters-{{$request['id']}}').on('show.bs.collapse', function () {
                    document.getElementById('details-toggle-body-parameters-{{$request['id']}}').innerHTML = "Hide"
                })
            </script>
        @else
            <p>No parameters</p>
        @endif


    <!-- RESULT -->
        <?php
        $result = json_decode($request["query_result"], true);

        $responseData = null;
        if(array_key_exists('data', $result)) {
            $responseData = \App\Services\UIHelperService::dataArrayToTable($result['data']);
        }
        ?>
        <h4>Result
            @if($result)
                <i><a style="font-size: 75%;" id="details-toggle-result-{{$request['id']}}" data-toggle="collapse" href="#collapse-result-{{$request['id']}}" aria-expanded="false" aria-controls="collapse-result-{{$request['id']}}">
                        Hide
                    </a></i>
            @endif
        </h4>

        @if($result)
            <div class="collapse in" id="collapse-result-{{$request['id']}}">
                <div>
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#table-view-{{ $request["id"] }}" aria-controls="table-view-{{ $request["id"] }}" role="tab" data-toggle="tab">Data</a></li>
                        <li role="presentation"><a href="#json-view-{{ $request["id"] }}" aria-controls="json-view-{{ $request["id"] }}" role="tab" data-toggle="tab">JSON</a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="table-view-{{ $request["id"] }}">

                            @if($responseData)
                                {!! $responseData !!}
                            @else
                                @if($result['success'])
                                    <h4 style="color: green">Success</h4>
                                @else
                                    <h4 style="color: red">Failure</h4>
                                    <p>{{ $result['error'] }}</p>
                                @endif
                            @endif
                        </div>

                        <div role="tabpanel" class="tab-pane" id="json-view-{{ $request["id"] }}">
                            @include('utils.json_viewer', ['json' => $request["query_result"], 'empty_json_message' => 'Nothing to show...', 'id' => 'result' . $request["id"]])
                        </div>
                    </div>
                </div>
            </div>

            <script>
                $('#collapse-result-{{$request['id']}}').on('hide.bs.collapse', function () {
                    document.getElementById('details-toggle-result-{{$request['id']}}').innerHTML = "Show"
                })

                $('#collapse-result-{{$request['id']}}').on('show.bs.collapse', function () {
                    document.getElementById('details-toggle-result-{{$request['id']}}').innerHTML = "Hide"
                })
            </script>
        @else
            <p>Nothing to show...</p>
        @endif
    </div>
</div>