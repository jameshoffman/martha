@extends('layouts.master')

<style>
    .collapsible-heading .chevron:after {
        content: "\f078";
    }
    .collapsible-heading.collapsed .chevron:after {
        content: "\f054";
    }
</style>

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="pull-left">API Requests</h1>

                {{ Form::open(['url' => action('ApiRequestsController@purge')]) }}
                {{ Form::hidden('_method', 'DELETE') }}
                {{ Form::submit('Purge API requests', array('class' => 'btn btn-danger btn-sm pull-right', 'style' => 'margin-top: 25px')) }}
                {{ Form::close() }}
                <div class="clearfix"></div>

                <hr style="margin-top: 0px"/>
            </div>
        </div>

        <div class="row">
            {{ Form::open(['method' => 'GET', 'url' => action('ApiRequestsController@index'), 'role' => 'search']) }}
            <div class="col-xs-12">
                    <div class="input-group">
                        @if($search)
                            <span class="input-group-btn">
                                <a href="{{ action("ApiRequestsController@index") }}" class="btn btn-danger" role="button">Reset filter</a>
                            </span>
                        @endif

                        {!! Form::text('search', $search, [ 'class'=> 'form-control', 'placeholder' => 'Search...']) !!}

                        <span class="input-group-btn">
                            {{ Form::submit('Filter', array('class' => 'btn btn-default btn-block', 'role' => 'button')) }}
                        </span>
                    </div>
            </div>
            {{ Form::close() }}
        </div>

        <div class="row" style="padding-top: 20px">
            <div class="col-md-12">
                @forelse($apiRequests as $request)
                    @include('api-requests.index_row_api_request', compact('request'))
                @empty
                    <h4>No requests to show...</h4>
                @endforelse
            </div>
        </div>
    </div>
@endsection