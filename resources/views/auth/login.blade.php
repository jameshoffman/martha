@extends('layouts.master')

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <img src="/logo.png" width="100%">
                <!-- <h1 class="text-center">Login</h1> -->
                <hr/>

                @include('errors.list')

                {!! Form::open(['action' => 'Auth\LoginController@login']) !!}

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    {!! Form::label('username', 'Username') !!}
                    {!! Form::text('username', null, ['class'=> 'form-control', 'required' => 'required', 'autofocus' => 'autofocus']) !!}
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    {!! Form::label('password', 'Password') !!}
                    {!! Form::password('password', ['class'=> 'form-control', 'required' => 'required']) !!}
                </div>

                <div class="form-group text-center">
                    {!! Form::checkbox('remember', old('remember') ? 'checked' : '') !!}
                    {!! Form::label('remember', 'Remember me') !!}
                </div>


                {!! Form::submit('Log in', ['class' =>'btn btn-primary btn-block']) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>

@endsection