<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Martha</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                @if(Auth::user()->is_admin)
                    <li>{!! link_to_action('Admin\UsersController@index', $title = 'Users') !!}</li>
                @endif
                <li>{!! link_to_action('Queries\QueryBuilderController@index', $title = 'Query Builder') !!}</li>
                <li>{!! link_to_action('Queries\QueriesController@index', $title = 'Queries') !!}</li>
                <li>{!! link_to_action('TablesController@index', $title = 'Tables') !!}</li>
                <li>{!! link_to_action('ApiRequestsController@index', $title = 'API Requests') !!}</li>
                <li>{!! link_to_action('AboutController@index', $title = 'About') !!}</li>
            </ul>

            <p class="navbar-text navbar-right">
                Hi {!! Auth::user()->display_name .  ' [' . Auth::user()->username .']' !!},
                {!! link_to_action('Auth\LoginController@logout', $title = 'Log out') !!}
            </p>

        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>