<!-- ['user'=> ?] -->
{!! Form::open(['url' => action('Admin\UsersController@update', ['id' => $user->id])]) !!}
{{ Form::hidden('_method', 'PATCH') }}

<div class="row">
    <div class="col-md-4 form-group{{ $errors->has('display_name') ? ' has-error' : '' }}">
        <label for="display_name">Display name</label>
        {!! Form::text('display_name', $user->display_name, [  'class'=> 'form-control',
                                                'placeholder' => 'Display name',
                                                'required' => 'required']) !!}
    </div>

    <div class="col-md-4 form-group{{ $errors->has('username') ? ' has-error' : '' }}">
        <label for="username">Username</label>
        {!! Form::text('username', $user->username, [  'class'=> 'form-control',
                                            'placeholder' => 'Username',
                                            'required' => 'required']) !!}
    </div>

    <div class="col-md-4 form-group{{ $errors->has('password') ? ' has-error' : '' }}">
        <label for="new_password">New password</label>
        {!! Form::text('new_password', null, [  'class'=> 'form-control',
                                            'placeholder' => 'New password']) !!}
    </div>
</div>

<div class="row">
    <div class="col-md-4 form-group{{ $errors->has('database_name') ? ' has-error' : '' }}">
        <label for="database_name">Database Name</label>
        {!! Form::text('database_name', $user->database_name, [  'class'=> 'form-control',
                                                'placeholder' => 'Database name',
                                                'readonly']) !!}
    </div>

    <div class="col-md-4 form-group{{ $errors->has('database_username') ? ' has-error' : '' }}">
        <label for="database_username">Database Username</label>
        {!! Form::text('database_username', $user->database_username, [  'class'=> 'form-control',
                                            'placeholder' => 'Database Username',
                                            'readonly']) !!}
    </div>

    <div class="col-md-4 form-group{{ $errors->has('database_password') ? ' has-error' : '' }}">
        <label for="database_password">Database Password</label>
        {!! Form::text('database_password', $user->database_password, [  'class'=> 'form-control',
                                            'placeholder' => 'Database Password',
                                            'readonly']) !!}
    </div>
</div>

<div class="row">
    <div class="col-md-2 col-md-offset-8">
        {{ link_to_action(  'Admin\UsersController@show', $title = 'Cancel', $parameters = [$user->id],
                            $attributes = ['class' => 'btn btn-default btn-block']) }}
    </div>

    <div class="col-md-2">
        {!! Form::submit('Save', ['class' =>'btn btn-primary btn-block']) !!}
    </div>
</div>

{!! Form::close() !!}