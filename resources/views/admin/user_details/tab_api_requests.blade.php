<!-- ['user'=> ?] -->
<div class="pull-right data-action-buttons-container">
    {{ Form::open(['url' => action('Admin\UserDetailsController@purgeApiRequests', ['id' => $user->id])]) }}
    {{ Form::hidden('_method', 'DELETE') }}
    {{ Form::submit('Purge API requests', array('class' => 'btn btn-danger pull-right')) }}
    {{ Form::close() }}
</div>

<div>
    {{ Form::open(['method' => 'GET', 'url' => action('Admin\UsersController@show', ['id' => $user->id]), 'role' => 'search']) }}
    <div class="input-group">
        @if($api_requests_search)
            <span class="input-group-btn">
                <a href="{{ action('Admin\UsersController@show', ['id' => $user->id, 'panel' => 'api-requests']) }}" class="btn btn-danger" role="button">Clear</a>
            </span>
        @endif

        {!! Form::hidden('panel', 'api-requests') !!}
        {!! Form::text('api-requests-search', $api_requests_search, [ 'class'=> 'form-control',
                                            'placeholder' => 'Search...']) !!}
        <span class="input-group-btn">
            {{ Form::submit('Search!', array('class' => 'btn btn-default', 'role' => 'button')) }}
        </span>
    </div>
    {{ Form::close() }}
</div>

<div class="row">
    <div class="col-md-12">
        @forelse($apiRequests as $request)
            @include('api-requests.index_row_api_request', compact('request'))
        @empty
            <h4>No requests to show...</h4>
        @endforelse
    </div>
</div>