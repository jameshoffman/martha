<!-- ['user'=> ?] -->
<div class="pull-right data-action-buttons-container">
    {{ Form::open(['url' => action('Admin\UserDetailsController@exportQueries', ['id' => $user->id]), 'style' => 'margin-bottom: 0;']) }}
    {{ Form::hidden('_method', 'POST') }}
    <button type="button" class="btn btn-success" data-toggle="modal" data-target=".import-queries-modal">Import</button>
{{ Form::submit('Export', array('class' => 'btn btn-info')) }}
{{ Form::close() }}

<!-- Import modal -->
    <div class="modal fade import-queries-modal" tabindex="-1" role="dialog" aria-labelledby="importQueriesModal">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="importQueriesModal">Import queries</h4>
                </div>

                {{ Form::open(['url' => action('Admin\UserDetailsController@importQueries', ['id' => $user->id]), 'files' => true, 'style' => 'margin-bottom: 0;']) }}
                {{ Form::hidden('_method', 'POST') }}
                <div class="modal-body">
                    {{ Form::file('queries_file') }}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    {{ Form::submit('Import', array('class' => 'btn btn-success')) }}
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
    <!-- -->
</div>

<div>
    {{ Form::open(['method' => 'GET', 'url' => action('Admin\UsersController@show', ['id' => $user->id])]) }}
    <div class="input-group">
        @if($queries_search)
            <span class="input-group-btn">
                <a href="{{ action("Admin\UsersController@show", ['id' => $user->id, 'panel' => 'queries']) }}" class="btn btn-danger" role="button">Clear</a>
            </span>
        @endif
        {!! Form::hidden('panel', 'queries') !!}

        {!! Form::text('queries-search', $queries_search, [ 'class'=> 'form-control', 'placeholder' => 'Search...']) !!}
        <span class="input-group-btn">
            {{ Form::submit('Search!', array('class' => 'btn btn-default', 'role' => 'button')) }}
        </span>
    </div>
    {{ Form::close() }}
</div>

@if($queries->isEmpty())
    <h3 class="text-center">No queries...</h3>
@else
    <!-- Table -->
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
            <tr>
                <th class="col-xs-2 col-md-2">Type</th>
                <th class="col-xs-3 col-md-2">Name</th>
                <th class="col-xs-6 col-md-8">Query</th>
            </tr>
            </thead>
            <tbody>
            @foreach($queries as $key => $query)
                <tr>
                    <td><b>{{ strtoupper($query->typeName) }}</b></td>
                    <td>{{ substr($query->name, 0, 40) }}</td>
                    <td><span class="sql-code-preview">{!! nl2br($query->string) !!}</span></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endif