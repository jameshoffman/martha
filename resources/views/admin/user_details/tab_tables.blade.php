<!-- ['user'=> ?] -->
<div class="pull-right data-action-buttons-container">
    {{ Form::open(['url' => action('Admin\UserDetailsController@mySqlDump', ['id' => $user->id])]) }}
    {{ Form::hidden('_method', 'POST') }}
    <button type="button" class="btn btn-success" data-toggle="modal" data-target=".import-tables-modal">Import MySQL Dump</button>
{{ Form::submit('Export MySQL Dump', array('class' => 'btn btn-info')) }}
{{ Form::close() }}

<!-- Import modal -->
    <div class="modal fade import-tables-modal" tabindex="-1" role="dialog" aria-labelledby="importMySqlDumpModal">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Import MySQL Dump</h4>
                </div>

                {{ Form::open(['url' => action('Admin\UserDetailsController@importMySqlDump', ['id' => $user->id]), 'files' => true, 'style' => 'margin-bottom: 0;']) }}
                {{ Form::hidden('_method', 'POST') }}
                <div class="modal-body">
                    {{ Form::file('mysqldump_file') }}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    {{ Form::submit('Import', array('class' => 'btn btn-success')) }}
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
    <!-- -->
</div>
<div>
    {{ Form::open(['method' => 'GET', 'url' => action('Admin\UsersController@show', ['id' => $user->id]), 'role' => 'search']) }}
    <div class="input-group">
        @if($tables_search)
            <span class="input-group-btn">
                <a href="{{ action('Admin\UsersController@show', ['id' => $user->id, 'panel' => 'tables']) }}" class="btn btn-danger" role="button">Clear</a>
            </span>
        @endif
        {!! Form::hidden('panel', 'tables') !!}

        {!! Form::text('tables-search', $tables_search, ['class'=> 'form-control',
                                                         'placeholder' => 'Search...']) !!}
        <span class="input-group-btn">
            {{ Form::submit('Search!', array('class' => 'btn btn-default', 'role' => 'button')) }}
        </span>
    </div>
    {{ Form::close() }}
</div>

<div class="row">
    <div class="col-md-12">
        @if(empty($tables))
            <h4>No tables to show...</h4>
        @else
            @foreach($tables as $key => $table)
                @include('tables.index_row_table', compact('table'))
            @endforeach
        @endif
    </div>
</div>