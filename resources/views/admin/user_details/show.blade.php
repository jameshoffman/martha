@extends('layouts.master')

<style>
    .tab-pane {
        padding-top: 10px;
    }

    .data-action-buttons-container {
        padding-left: 15px;
    }
</style>

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>User details: {{$user->display_name}}</h1>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                @include('errors.list')

                @include('layouts.flash')
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Credentials</h3>
                    </div>
                    <div class="panel-body">
                        @include('admin.user_details.form_update', ['user' => $user])
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Data</h3>
                    </div>
                    <div class="panel-body">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="{{$panel == 'query-builder' ? 'active' : ''}}"><a href="#query-builder-view" aria-controls="query-builder-view" role="tab" data-toggle="tab">Query Builder</a></li>
                            <li role="presentation" class="{{$panel == 'queries' ? 'active' : ''}}"><a href="#queries-view" aria-controls="queries-view" role="tab" data-toggle="tab">Queries</a></li>
                            <li role="presentation" class="{{$panel == 'tables' ? 'active' : ''}}"><a href="#tables-view" aria-controls="tables-view" role="tab" data-toggle="tab">Tables</a></li>
                            <li role="presentation" class="{{$panel == 'api-requests' ? 'active' : ''}}"><a href="#api-requests-view" aria-controls="api-requests-view" role="tab" data-toggle="tab">API Requests</a></li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane {{$panel == 'query-builder' ? 'active' : ''}}" id="query-builder-view">
                                @include('admin.user_details.tab_query_builder', ['user' => $user])
                            </div>

                            <div role="tabpanel" class="tab-pane {{$panel == 'queries' ? 'active' : ''}}" id="queries-view">
                                @include('admin.user_details.tab_queries', ['user' => $user])
                            </div>

                            <div role="tabpanel" class="tab-pane {{$panel == 'tables' ? 'active' : ''}}" id="tables-view">
                                @include('admin.user_details.tab_tables', ['user' => $user])
                            </div>

                            <div role="tabpanel" class="tab-pane {{$panel == 'api-requests' ? 'active' : ''}}" id="api-requests-view">
                                @include('admin.user_details.tab_api_requests', ['user' => $user])
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
