<!-- ['user'=> ?] -->
{!! Form::open(['action' => ['Admin\UserDetailsController@executeQuery', $user->id]]) !!}

<div class="row">
    <div class="col-xs-12 form-group">
        @include('utils.sql_editor', ['text_area_name' => 'query', 'text_area_id' => 'query_textarea'])
    </div>
</div>

<div class="row form-group">
    <div class="col-xs-12 {{ $errors->has('parameters') ? ' has-error' : '' }}">
        {!! Form::textarea('parameters', null, ['class'=> 'form-control',
                                                'placeholder' => 'Parameters JSON',
                                                'rows' => '2',
                                                'style' => 'max-width: 100%;']) !!}
    </div>
</div>

<div class="row form-group">
    <div class="col-xs-6">
        {!! Form::submit('Execute statement', ['name' => 'statement', 'class' =>'btn btn-info btn-block']) !!}
    </div>

    <div class="col-xs-6">
        {!! Form::submit('Execute select', ['name' => 'select', 'class' =>'btn btn-success btn-block']) !!}
    </div>
</div>
{!! Form::close() !!}

<hr />

<h4>Result</h4>
@include('query.result', ['queryRunResponse' => session('queryRunResponse')])