@extends('layouts.master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Users</h1>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Add user</h3>
                    </div>
                    <div class="panel-body">

                        @include('errors.list')

                        @include('layouts.flash')

                        {!! Form::open(['action' => 'Admin\UsersController@store']) !!}
                        <!-- <div class="row">
                            <div class="col-md-4 form-group">
                                <label>User credentials</label>
                            </div>
                        </div> -->

                        <div class="row">
                            <!-- <div class="col-md-4 form-group{{ $errors->has('display_name') ? ' has-error' : '' }}">
                                {!! Form::text('display_name', null, [  'class'=> 'form-control',
                                                                        'placeholder' => 'Display name',
                                                                        'required' => 'required']) !!}
                            </div> -->

                            <div class="col-md-6 form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                                {!! Form::text('username', null, [  'class'=> 'form-control',
                                                                    'placeholder' => 'Username',
                                                                    'required' => 'required']) !!}
                            </div>

                            <div class="col-md-6 form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                {!! Form::text('password', null, [  'class'=> 'form-control',
                                                                    'placeholder' => 'Password',
                                                                    'required' => 'required']) !!}
                            </div>
                        </div>

                        <!-- <div class="row">
                            <div class="col-md-4 form-group">
                                <label>Database credentials</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4 form-group{{ $errors->has('database_name') ? ' has-error' : '' }}">
                                {!! Form::text('database_name', null, [  'class'=> 'form-control',
                                                                        'placeholder' => 'Database name',
                                                                        'required' => 'required']) !!}
                            </div>

                            <div class="col-md-4 form-group{{ $errors->has('database_username') ? ' has-error' : '' }}">
                                {!! Form::text('database_username', null, [  'class'=> 'form-control',
                                                                    'placeholder' => 'Database Username',
                                                                    'required' => 'required']) !!}
                            </div>

                            <div class="col-md-4 form-group{{ $errors->has('database_password') ? ' has-error' : '' }}">
                                {!! Form::text('database_password', null, [  'class'=> 'form-control',
                                                                    'placeholder' => 'Database Password',
                                                                    'required' => 'required']) !!}
                            </div>
                        </div> -->

                        <div class="row">
                            <div class="col-md-2">
                                <label> {!! Form::checkbox('external', 'yes') !!} Allow external login?</label>
                            </div>
                            <div class="col-md-2 col-md-offset-6">
                                {{ link_to_action(  'Admin\UsersController@index', $title = 'Cancel', $parameters = [],
                                                    $attributes = ['class' => 'btn btn-default btn-block']) }}
                            </div>

                            <div class="col-md-2">
                                {!! Form::submit('Add', ['class' =>'btn btn-primary btn-block']) !!}
                            </div>
                        </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">List</h3>
                    </div>
                    <div class="panel-body">
                        <div>
                            {{ Form::open(['method' => 'GET', 'url' => action('Admin\UsersController@index'), 'role' => 'search']) }}
                            <div class="input-group">
                                @if($search)
                                    <span class="input-group-btn">
                                        <a href="{{ action("Admin\UsersController@index") }}" class="btn btn-danger" role="button">Clear</a>
                                    </span>
                                @endif

                                {!! Form::text('search', $search, [ 'class'=> 'form-control',
                                                                    'placeholder' => 'Search...']) !!}
                                <span class="input-group-btn">
                                    {{ Form::submit('Search!', array('class' => 'btn btn-default', 'role' => 'button')) }}
                                </span>
                            </div>
                            {{ Form::close() }}
                        </div>
                    </div>

                    @if($users->isEmpty())
                        <h3 class="text-center">No users...</h3>
                    @else
                        <!-- Table -->
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Display Name</th>
                                    <th>Username</th>
                                    <th style="width: 175px;">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($users as $key => $value)
                                    <tr>
                                        <td>{{ $value->display_name }}</td>
                                        <td>{{ $value->username }}</td>

                                        <td>
                                            <a class="btn btn-small btn-warning" href="{{ URL::to('users/' . $value->id) }}">Details</a>

                                            {{ Form::open(['url' => action('Admin\UsersController@destroy', ['id' => $value->id]), 'class' => 'pull-right']) }}
                                            {{ Form::hidden('_method', 'DELETE') }}
                                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                                            {{ Form::close() }}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
