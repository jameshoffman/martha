# Martha

## Bugs
- Verify dry-run in QueriesService, the rollback is not applied(autocommit?)
- Only numbers in username fails???
- Detect error in JSON params
- In users.php, be carefull show tables(line ~70) also shows VIEWS, it crashes at line 92 because show create does not exit on a VIEW-> show full tables where Table_Type = 'BASE TABLE'
- Bind parameters to prevent SQL Injection

## Features
- Include an authentication proxy to avoid client-side username:password
- use websocket to automaticaly refresh api requests list
- CRUDL queries generator
- Api queries/routes documentation
- return inserted item id (QueryType Table ?)
- use basic auth
- Remove external dependencies
- Add reload buttons in user�s detail panels to refresh and stay in same panel
- Readme + setup
- ; in query other than to end line makes query fail(regex not between quotes)
- add delete confirmation dialogs
- fix remember me login
- 2 step search: full string, then append current search strategy(split on space) to prioritize results