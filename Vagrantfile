# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
    config.vm.box = "debian/bullseye64"

    config.vm.network "forwarded_port", guest: 80, host: 8765

    config.vm.provider "virtualbox" do |vb|
        vb.memory = "2048"
    end

    # Cannot share directly source/hexibits because permissions were attributed to root instead of vagrant user
    config.vm.synced_folder ".", "/vagrant", type: "virtualbox"
  
    #
    # Provisioning
    #
  
    config.vm.provision "shell", inline: <<-'SHELL_PROVISON'
        # Upgrade needs to be completely non-interactive
        apt update && DEBIAN_FRONTEND=noninteractive apt -y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" upgrade

        # Utils, unzip is used by composer
        apt install -y curl vim unzip
        
        # Database
        apt install -y mariadb-server
    
        mysql --user=root <<-'EOF_MYSQL'
            CREATE DATABASE marthadb;
            GRANT ALL PRIVILEGES on marthadb.* to 'marthaapp'@'localhost' IDENTIFIED BY '@_passw0rd';
EOF_MYSQL

        # Apache
        apt install -y apache2
        sed -i 's+DocumentRoot /var/www/html+DocumentRoot /vagrant/public+g' /etc/apache2/sites-enabled/000-default.conf 
        
        a2enmod rewrite
        cat <<EOHTML >> /etc/apache2/apache2.conf
<Directory /vagrant/public>
    Options Indexes FollowSymLinks
    AllowOverride All
    Require all granted
</Directory>
EOHTML
        
        apachectl restart

        # PHP
        apt install -y php php-common php-cli php-intl php-mbstring php-mysql php-bcmath php-json php-tokenizer php-xml php-simplexml php-zip
        
        # Composer
        cd ~ && curl -sS https://getcomposer.org/installer -o composer-setup.php
        php composer-setup.php --install-dir=/usr/local/bin --filename=composer

        # Lavavel
        chmod 757 -R /vagrant/storage

        su - vagrant -c 'composer global require laravel/installer'
        su - vagrant -c "echo 'export PATH=$PATH:/home/vagrant/.config/composer/vendor/bin' >> /home/vagrant/.profile"
        su - vagrant -c 'cd /vagrant && composer install -n'
        su - vagrant -c 'cd /vagrant && cp .env.example .env'
        su - vagrant -c 'cd /vagrant && php artisan key:generate'
        su - vagrant -c 'cd /vagrant && php artisan migrate && php artisan db:seed'
        
SHELL_PROVISON
  
    #
    # UP trigger
    #
  
    config.trigger.after [:up, :reload] do |trigger|
        trigger.info = "Starting DEV server!"
        trigger.run_remote = {inline: <<-'SHELL_TRIGGER'
            su - vagrant -c 'cd /vagrant && composer install -n'
            su - vagrant -c 'php /vagrant/artisan migrate:refresh --seed'
SHELL_TRIGGER
        }
    end
  end
  