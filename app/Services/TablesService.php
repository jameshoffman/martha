<?php

namespace App\Services;


use App\Model\Query;
use App\Model\User;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;

class TablesService {

    public static function mySqlDumpForUser(User $user, $onErrorRedirectAction) {
        $mysqlDumpPath = 'mysqldump';
        if (env('APP_ENV') == 'ampps-local') {
            $mysqlDumpPath = '/Applications/AMPPS/mysql/bin/mysqldump';
        }

        exec( '(' . $mysqlDumpPath . ' -u ' . $user->database_username .
                        ' -p' . $user->database_password . ' '
                        . $user->database_name . ' --routines --triggers) 2>&1', $output, $result);

        $headers = [
            'Content-type'        => 'text/plain',
            'Content-Disposition' => 'attachment; filename="mysqldump_' . date('Y-m-d_H:i:s') . '.sql"',
        ];

        if (count($output) > 0) {
            if (str_contains($output[0], "insecure")) {
                unset($output[0]);
            }

            $dump = implode("\n", $output);

            return Response::make($dump, 200, $headers);
        } else {
            return Redirect::to($onErrorRedirectAction)->withErrors(['error' => 'Could not generate MySQL Dump.']);
        }
    }

    public static function importMySqlDumpForUser($fileInputName, User $user, $redirectAction) {
        $file = Input::file($fileInputName);

        if ($file) {
            $fileContent = file_get_contents($file);

            if (empty($fileContent) == false) {
                $query = new Query();
                $query->string = $fileContent;
                $query->typeName = 'statement';

                $response = QueriesService::run($query, $user);

                if ($response['success']) {
                    Session::flash('message', 'Imported MySQL Dump successfully!');
                    return Redirect::to($redirectAction);
                } else {
                    $errors = [ 'error' => 'Failed to import MySQL Dump.',
                                'response' => $response['error']];
                    return Redirect::to($redirectAction)->withErrors($errors);
                }
            } else {
                return Redirect::to($redirectAction)->withErrors(['error' => 'MySQL Dump file is empty.']);
            }
        } else {
            return Redirect::to($redirectAction)->withErrors(['error' => 'No MySQL Dump file provided.']);
        }
    }
}