<?php

namespace App\Services;

use App\Model\Query;
use App\Model\Responses\ImportQueriesResponse;
use App\Model\User;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use PDO;

class QueriesService
{
    public static function run(Query $query, User $user, array $params = []) {
        $connection = $user->databaseConnection;

        $response = [];

        if ($query->type == 1) { //statement
            try {
                $queryString = $query->parameterized($params);

                $queries = explode(';', $queryString);
                $errors = array();

                /* Commented out because no longer works...
                //Start a transaction to run queries in dry mode and verify errors
                $connection->beginTransaction();
                foreach ($queries as $q) {
                    //ignore all transaction keywords because we are in dry-run, we always want to rollback
                    if (empty(trim($q)) == false
                        && stripos(strtolower($q), 'begin transaction') === false
                        && stripos(strtolower($q), 'commit') === false
                        && stripos(strtolower($q), 'rollback') === false)
                    {
                        try {
                            $connection->unprepared($q);
                        } catch (Exception $e) {
                            $errors[] = preg_replace('#\R+#', '', $e->getMessage());
                        }
                    }
                }
                $connection->rollBack();
                */
                
                if (count($errors) == 0) {
                    //No errors found, perform query
                    $connection->unprepared($queryString);

                    //TODO: [hacky] fix it when query types are added
                    //preg_match('/insert into (\S*) /', $q, $matches);
                    $lastId = intval($connection->getPdo()->lastInsertId());
                    if (stripos($queryString, "insert") !== false && $lastId != 0) {
                        $response['lastInsertId'] = $lastId;
                    }

                    $response["success"] = true;
                } else {
                    $response["success"] = false;
                    $response["error"] = implode("\n", $errors);
                }
            } catch (Exception $e) {
                $response["success"] = false;
                $response["error"] = $e->getMessage();
            }
        } else {
            try {
                $response["data"] = $connection->select($query->parameterized($params));
                $response["success"] = true;
            } catch (Exception $e) {
                $response["success"] = false;
                $response["error"] = $e->getMessage();
            }
        }

        return $response;
    }

    public static function importForUser($fileInputName, User $user, $redirect) {
        $file = Input::file($fileInputName);

        if ($file) {
            $fileContent = file_get_contents($file);
            $parsedQueries = json_decode($fileContent, true);

            if ($parsedQueries) {
                $importResponse = QueriesService::import($parsedQueries, $user);

                $flashMessage = "Successfully inserted <b>$importResponse->totalQueryInserted</b> queries!";
                if (count($importResponse->duplicatesQueries) > 0) {
                    $flashMessage .= "<br /><br />";
                    $flashMessage .= "<b>*Warning* Following queries already exist(they were not inserted again):</b><br />";
                    $flashMessage .= implode(",<br />", $importResponse->duplicatesQueries);
                }

                Session::flash('message', $flashMessage);
                return Redirect::to($redirect)->withErrors($importResponse->errors);
            } else {
                return Redirect::to($redirect)->withErrors(['error' => 'Invalid queries file provided.']);
            }
        } else {
            return Redirect::to($redirect)->withErrors(['error' => 'No file provided.']);
        }
    }

    private static function import(array $queries, User $user) {
        $totalQueryInserted = 0;
        $errors = array();
        $duplicatesQueries = [];

        $query = null;
        foreach ($queries as $query) {
            try {
                $queryToImport = new Query($query);

                if ($queryToImport) {
                    $user->queries()->save($queryToImport);

                    $totalQueryInserted++;
                }
            }
            catch (QueryException $e) {
                if ($queryToImport) {
                    // name is valid when Query object could be created,
                    // but save failed(in most cases due to constraint violation on unique name)
                    $duplicatesQueries[] = $queryToImport->name;
                }
            }
            catch (Exception $e) {
                $errors[] = $e->getMessage();
            }
        }

        return new ImportQueriesResponse($totalQueryInserted, $duplicatesQueries, $errors);
    }

    public static function exportForUser(User $user, $onErrorRedirectAction) {
        $queries = $user->queries()->get(['name', 'string', 'type']);

        $headers = [
            'Content-type'        => 'text/plain',
            'Content-Disposition' => 'attachment; filename="queries_'. $user->username . date('_Y-m-d_H:i:s') . '.json"',
        ];

        if (count($queries) > 0) {
            return Response::make(json_encode($queries), 200, $headers);
        } else {
            return Redirect::to($onErrorRedirectAction)->withErrors(['error' => 'No queries to export.']);
        }
    }
}