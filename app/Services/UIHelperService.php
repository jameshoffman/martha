<?php

namespace App\Services;

class UIHelperService {
    public static function dataArrayToTable($data) {
        if (empty($data)) {
            $htmlTable = "";
        } else {
            $longDataDetailsToggleScript = '';

            $htmlTable = "<div class=\"table-responsive\">";
            $htmlTable .= "<table class=\"table table-bordered table-striped\">";
            $htmlTable .= "<thead>";
            $htmlTable .= "<tr>";

            foreach (array_keys(get_object_vars((object)$data[0])) as $key) {
                $htmlTable .= "<th>" . $key . "</th>";
            }

            $htmlTable .= "</tr>";
            $htmlTable .= "</thead>";
            $htmlTable .= "<tbody>";

            $items = json_decode(json_encode($data), TRUE);

            foreach ($items as $item) {
                $htmlTable .= "<tr>";
                foreach (array_keys($item) as $itemKey) {
                    $unique = uniqid();
                    $itemValue = nl2br($item[$itemKey]);
                    $maxLength = 200;

                    if (strlen($itemValue) < $maxLength) {
                        $htmlTable .= "<td>" . $item[$itemKey] . "</td>";
                    } else {
                        $tmpDataDetailsToggleScript = '';
                        $collapsedItemValue = substr($itemValue, 0, $maxLength);

                        $htmlTable .= "<td>";
                        $htmlTable .= "<div id=\"details-content-{$unique}\">{$collapsedItemValue}</div>";
                        $htmlTable .= "<span class=\"collapse\" id=\"collapse-item-{$unique}\"></span>";
                        $htmlTable .= "<a style=\"font-size: 75%;\" id=\"details-toggle-{$unique}\" data-toggle=\"collapse\" href=\"#collapse-item-{$unique}\" aria-expanded=\"false\" aria-controls=\"collapse-item-{$unique}\">";
                        $htmlTable .= "More >>";
                        $htmlTable .= "</a>";
                        $htmlTable .= "</td>";

                        $tmpDataDetailsToggleScript .= "<script>";
                        $tmpDataDetailsToggleScript .= "$('#collapse-item-{$unique}').on('hide.bs.collapse', function () {";
                        $tmpDataDetailsToggleScript .= "document.getElementById('details-content-{$unique}').innerHTML = `{$collapsedItemValue}`;";
                        $tmpDataDetailsToggleScript .= "document.getElementById('details-toggle-{$unique}').innerHTML = \"More >>\";";
                        $tmpDataDetailsToggleScript .= "});";
                        $tmpDataDetailsToggleScript .= "$('#collapse-item-{$unique}').on('show.bs.collapse', function () {";
                        $tmpDataDetailsToggleScript .= "document.getElementById('details-content-{$unique}').innerHTML = `{$itemValue}`;";
                        $tmpDataDetailsToggleScript .= "document.getElementById('details-toggle-{$unique}').innerHTML = \"<< Less\";";
                        $tmpDataDetailsToggleScript .= "});";
                        $tmpDataDetailsToggleScript .= "</script>";
                        $tmpDataDetailsToggleScript .= "";

                        $longDataDetailsToggleScript .= $tmpDataDetailsToggleScript;
                    }
                }
                $htmlTable .= "</tr>";
            }

            $htmlTable .= "</tbody>";
            $htmlTable .= "</table>";
            $htmlTable .= "</div>";

            $htmlTable .= $longDataDetailsToggleScript;
        }

        return $htmlTable;
    }
}