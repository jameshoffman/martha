<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class ApiAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $requestAuth =  explode(":", base64_decode($request->header('auth')));
        $username = $requestAuth[0];
        $password = $requestAuth[1];
        Auth::once(['username' => $username, 'password' => $password]);

        if (Auth::retrieveByCredentials(['username' => $username, 'password' => $password])) {
            return $next($request);
        }



        return ["success" => false,
                "error" => "Unauthorized."];
    }
}
