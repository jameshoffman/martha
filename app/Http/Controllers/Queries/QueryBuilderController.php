<?php

namespace App\Http\Controllers\Queries;

use App\Http\Controllers\Controller;
use App\Model\Query;
use App\Model\Responses\QueryRunResponse;
use App\Services\QueriesService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class QueryBuilderController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() { }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('query.query_builder');
    }

    public function execute(Request $request) {
        $queryString = $request['query'];

        if (empty($queryString)) {
            return  redirect(action('Queries\QueryBuilderController@index'))->
                    withInput()->
                    withErrors(['query' => 'Specify a query!']);
        } else {
            $user = Auth::user();

            $query = new Query();
            $query->string = $queryString;

            if ($request->has("statement-add") || $request->has("select-add")) {
                if ($request->has("statement-add")) {
                    $query->typeName = 'statement';
                } else {
                    $query->typeName = 'select';
                }

                $queryName = $request['name'];
                if (empty($queryName) == false) {
                    $query->name = $queryName;

                    $inputs = [ 'name' => $query->name,
                                'query' => $query->string,
                                'type' => $query->typeName];
                    $validator = Validator::make($inputs, Query::validationRules($user->id));

                    if ($validator->fails()) {
                        return Redirect::to(action('Queries\QueryBuilderController@index'))->withErrors($validator)->withInput();
                    } else {
                        $user->queries()->save($query);

                        Session::flash('message', "Successfully added query: $query->name.");
                        return redirect(action('Queries\QueryBuilderController@index'));
                    }
                } else {
                    return  redirect(action('Queries\QueryBuilderController@index'))->
                                withInput()->
                                withErrors(['name' => 'Specify a query name!']);
                }
            } else if ($request->has("statement") || $request->has("select")) {
                if ($request->has("statement")) {
                    $query->typeName = 'statement';
                } else {
                    $query->typeName = 'select';
                }

                $params = json_decode($request['parameters'], true);
                if ($params == null) {
                    $params = [];
                }
                $response = QueriesService::run($query, $user, $params);
                $queryRunResponse = new QueryRunResponse($response,
                                                         $query->parameterized($params),
                                                         $request['parameters']);

                return  redirect(action('Queries\QueryBuilderController@index'))
                            ->with('queryRunResponse', $queryRunResponse) //response stored in session to use redirect
                            ->withInput();
            }
        }
    }
}
