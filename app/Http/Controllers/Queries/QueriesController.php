<?php

namespace App\Http\Controllers\Queries;

use App\Http\Controllers\Controller;
use App\Model\ApiRequest;
use App\Model\Query;
use App\Model\Responses\QueryRunResponse;
use App\Model\User;
use App\Services\QueriesService;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class QueriesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() { }

    public function index(Request $request) {
        $search = $request->get('search');

        $queries = Auth::user()->getFilteredQueries($search);

        return View::make('query.queries.index', compact(['queries', 'search']));
    }

    public function store(Request $request) {
        $inputs = $request->all();

        $user = Auth::user();
        $validator = Validator::make($inputs, Query::validationRules($user->id));

        if ($validator->fails()) {
            return Redirect::to(action('Queries\QueriesController@index'))->withErrors($validator)->withInput();
        } else {
            $query = new Query();
            $query->name = $inputs['name'];
            $query->string = $inputs['query'];
            $query->typeName = $inputs['type'];
            $user->queries()->save($query);

            Session::flash('message', "Successfully created query: $query->name.");
            return Redirect::to(action('Queries\QueriesController@index'));
        }
    }

    public function update(Request $request, $id) {
        $inputs = $request->all();

        if ($request->has('run')) {
            $queryToRun = new Query();

            $queryToRun->string = $inputs['query'];
            $queryToRun->typeName = $inputs['type'];

            $params = json_decode($request['parameters'], true);
            if ($params == null) {
                $params = [];
            }

            $response = QueriesService::run($queryToRun, Auth::user(), $params);
            $queryRunResponse = new QueryRunResponse($response,
                                                     $queryToRun->parameterized($params),
                                                     $request['parameters']);

            Session::flash('oldQuery', $queryToRun->string);
            Session::flash('queryRunResponse', $queryRunResponse);

            return  redirect(action('Queries\QueriesController@show', ['id'=> $id]))
                        ->withInput();
        } else {
            $user = Auth::user();
            $validator = Validator::make($inputs, Query::validationRules($user->id, $id));

            if ($validator->fails()) {
                return Redirect::to(action('Queries\QueriesController@show', ['id'=> $id]))->withErrors($validator)->withInput();
            } else {
                $query = $user->queries()->findOrFail($id);
                $query->name = $inputs['name'];
                $query->string = $inputs['query'];
                $query->typeName = $inputs['type'];
                $query->save();

                Session::flash('message', 'Successfully updated query!');
                return redirect()->action('Queries\QueriesController@show', ['id'=>$query->id]);
            }
        }
    }

    public function show($id) {
        $query = Auth::user()->queries()->findOrFail($id);

        return View::make('query.queries.show', compact('query'));
    }

    public function destroy($id) {
        $query =  Auth::user()->queries()->findOrFail($id);
        $queryName = $query->name;

        $query->delete();

        Session::flash('message', "Successfully deleted query: $queryName.");
        return Redirect::to(action('Queries\QueriesController@index'));
    }

    public function import() {
        return QueriesService::importForUser('queries_file',
                                             Auth::user(),
                                             action('Queries\QueriesController@index'));
    }

    public function export() {
        $user = Auth::user();

        return QueriesService::exportForUser($user, action('Queries\QueriesController@index'));
    }

    public function execute(Request $request, $name) {
        $bodyContent = $request->getContent();
        $queryString = '';
        $queryParameterizedString = '';
        $queryResult = '';

        $requestAuth = explode(":", base64_decode($request->header('auth')));

        if (count($requestAuth) == 2) {
            $username = $requestAuth[0];
            $password = $requestAuth[1];

            $user = User::where('username', $username)->first();

            if ($user != null && Hash::check($password, $user->password)) {
                $query = $user->queryNamed($name);

                if ($query) {
                    $params = json_decode($bodyContent, true);
                    if ($params == null) {
                        $params = [];
                    }

                    $queryString = $query->string;
                    $queryParameterizedString = $query->parameterized($params);

                    $queryResult = QueriesService::run($query, $user, $params);
                } else {
                    $queryResult = ["success" => false,
                        "error" => "Query does not exist."
                    ];
                }

                //Save to ApiRequests
                $apiRequest = new ApiRequest();
                $apiRequest->body_parameters = $bodyContent;
                $apiRequest->query_name = $name;
                $apiRequest->query_string = $queryString;
                $apiRequest->query_parameterized_string = $queryParameterizedString;
                $apiRequest->query_result = json_encode($queryResult);

                $user->apiRequests()->save($apiRequest);
            } else {
                $queryResult = ["success" => false,
                    "error" => "Invalid credentials."
                ];
            }
        } else {
            $queryResult = ["success" => false,
                "error" => "Invalid auth header."
            ];
        }

        return $queryResult;
    }
}
