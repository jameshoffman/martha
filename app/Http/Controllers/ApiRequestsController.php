<?php

namespace App\Http\Controllers;

use App\Model\ApiRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;

class ApiRequestsController extends Controller
{
    public function index(Request $request) {
        $search = $request->get("search");
        $apiRequests = Auth::user()->getFilteredApiRequests($search);

        return View::make('api-requests.index', compact(['apiRequests', 'search']));
    }

    public function purge() {
        Auth::user()->apiRequests()->delete();

        Session::flash('message', "Successfully purged api requests");
        return redirect(action('ApiRequestsController@index'));
    }
}