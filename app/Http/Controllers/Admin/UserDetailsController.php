<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Queries\QueriesController;
use App\Model\Query;
use App\Model\Responses\QueryRunResponse;
use App\Model\User;
use App\Services\QueriesService;
use App\Services\TablesService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;


class UserDetailsController extends Controller
{
    /*
     * Query builder
     */

    public function executeQuery(Request $request, $id) {
        $queryString = $request['query'];

        if (empty($queryString)) {
            return  redirect(action('Admin\UsersController@show', ['id'=> $id, 'panel' => 'query-builder']))
                ->withInput()
                ->withErrors(['query' => 'Specify a query!']);
        } else {
            $user = User::findOrFail($id);

            $query = new Query();
            $query->string = $queryString;

            if ($request->has("statement")) {
                $query->typeName = 'statement';
            } else {
                $query->typeName = 'select';
            }

            $params = json_decode($request['parameters'], true);
            if ($params == null) {
                $params = [];
            }
            $response = QueriesService::run($query, $user, $params);
            $queryRunResponse = new QueryRunResponse($response,
                                                     $query->parameterized($params),
                                                     $request['parameters']);

            return  redirect(action('Admin\UsersController@show', ['id'=> $id, 'panel' => 'query-builder']))
                        ->with('queryRunResponse', $queryRunResponse)
                        ->withInput();
        }
    }

    /*
     * Queries
     */

    public function importQueries(Request $request, $id) {
        $user = User::findOrFail($id);

        return QueriesService::importForUser('queries_file',
                                             $user,
                                             action('Admin\UsersController@show', ['id' => $id, 'panel' => 'queries']));
    }

    public function exportQueries(Request $request, $id) {
        $user = User::findOrFail($id);

        return QueriesService::exportForUser($user, action('Admin\UsersController@show', ['id' => $id, 'panel' => 'queries']));
    }

    /*
     * Tables
     */

    public function mySqlDump(Request $request, $id) {
        $user = User::findOrFail($id);

        return TablesService::mySqlDumpForUser($user, action('Admin\UsersController@show', ['id' => $id, 'panel' => 'tables']));
    }

    public function importMySqlDump(Request $request, $id) {
        $user = User::findOrFail($id);

        return TablesService::importMySqlDumpForUser('mysqldump_file',
                                                     $user,
                                                     action('Admin\UsersController@show', ['id' => $id, 'panel' => 'tables']));
    }

    /*
     * API Requests
     */

    public function purgeApiRequests(Request $request, $id) {
        $user = User::findOrFail($id);
        $user->apiRequests()->delete();

        Session::flash('message', "Successfully purged <b>$user->display_name</b>'s api requests");
        return redirect(action('Admin\UsersController@show', ['id' => $id, 'panel' => 'api-requests']));
    }
}