<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Queries\QueriesController;
use App\Model\Query;
use App\Model\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class UsersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() { }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {

        $search = trim($request["search"]);

        $searchCriteria = '%'. $search .'%';
        $users = User::where('is_admin',false)->where(function ($query) use ($searchCriteria) {
            $query->where('display_name', 'like', $searchCriteria)
                ->orWhere('username', 'like', $searchCriteria)
                ->orWhere('database_name', 'like', $searchCriteria)
                ->orWhere('database_username', 'like', $searchCriteria);
        })->get();

        return View::make('admin.users', compact(['users', 'search']));
    }

    public function store(Request $request) {
        $inputs = $request->all();

        $rules = array(
            'username' => 'required|unique:users',
            'password' => ['required', 'min:12', 'regex:/^(?=.*\d)(?=.*[A-Z])(?=.*[a-z])(?=.*[^\w\d\s:])([^\s]){12,}$/'],
            // 'display_name' => 'required|unique:users|max:255',
            // 'database_name' => 'required',
            // 'database_username' => 'required',
            // 'database_password' => 'required'
        );
        $validator = Validator::make($inputs, $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to(action('Admin\UsersController@index'))->withErrors($validator)->withInput();
        } else {
            // store
            $user = new User();
            $user->display_name = $inputs['username'];
            $user->username     = $inputs['username'];
            $user->password     = $inputs['password'];
            $user->database_name = $inputs['username'];
            $user->database_username = $inputs['username'];
            $user->database_password = $inputs['password'];
            $user->save();
            
            $dbUserHost = $request->has('external') ? '%' : 'localhost';
            
            $errors = array();
            try {
                DB::statement(DB::raw("CREATE DATABASE $user->database_name  CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;"));
            } catch (Exception $e) {
                $errors["exception-create-database"] = $e->getMessage();
            }

            try {
                DB::statement("CREATE USER '$user->database_username'@'$dbUserHost' IDENTIFIED BY '$user->database_password';");
            } catch (Exception $e) {
                $errors["exception-create-user"] = $e->getMessage();
            }

            try {
                DB::statement("GRANT ALL ON $user->database_name.* TO '$user->database_username'@'$dbUserHost'");
            } catch (Exception $e) {
                $errors["exception-grant-all"] = $e->getMessage();
            }

            if (empty($errors)) {
                Session::flash('message', "Successfully created user $user->display_name.");

                return Redirect::to(action('Admin\UsersController@index'));
            } else {
                $errorMessage = array();
                $errorMessage['exception'] = "User $user->display_name was created, but the following exceptions happened:";
                $errors = array_merge($errorMessage, $errors);

                return Redirect::to(action('Admin\UsersController@index'))->
                        withErrors(["exception"=>$e->getMessage()])->
                        withInput();
            }
        }
    }

    public function show(Request $request, $id) {
        $user = User::findOrFail($id);

        $queries_search = $request->get('queries-search');
        $queries = $user->getFilteredQueries($queries_search);

        $tables_search = $request->get('tables-search');
        $tables = $user->getTables($tables_search);

        $api_requests_search = $request->get('api-requests-search');
        $apiRequests = $user->getFilteredApiRequests($api_requests_search);

        $panel = 'query-builder';
        if ($request->has('panel')) {
            $panelParam = $request->get('panel');
            $authorizedPanels = ['query-builder', 'queries', 'tables', 'api-requests'];

            if (in_array($panelParam, $authorizedPanels)) {
                $panel = $panelParam;
            }
        }

        return View::make('admin.user_details.show', compact(['user',
                                                            'queries', 'queries_search',
                                                            'tables', 'tables_search',
                                                            'apiRequests', 'api_requests_search',
                                                            'panel']));
    }

    public function update(Request $request, $id) {
        $user = User::findOrFail($id);
        $inputs = $request->all();

        $rules = array(
            'display_name' => "required|unique:users,display_name,$id|max:255",
            'username' => "required|unique:users,username,$id"
        );
        $validator = Validator::make($inputs, $rules);

        if ($validator->fails()) {
            return Redirect::to(action('Admin\UsersController@show', ['id'=> $id]))->withErrors($validator)->withInput();
        } else {
            $user->display_name = $inputs['display_name'];
            $user->username = $inputs['username'];

            if ($request->has('new_password') && empty($inputs["new_password"]) == false){
                $user->password = $inputs["new_password"];
            }

            $user->save();

            Session::flash('message', "Successfully updated user $user->display_name.");
            return Redirect::to(action('Admin\UsersController@show', ['id'=> $id]))->withErrors($validator)->withInput();
        }
    }

    public function destroy($id) {
        $user = User::findOrFail($id);

        $errors = array();
        try {
            DB::statement("DROP DATABASE $user->database_name;");
        } catch (Exception $e) {
            $errors["exception-drop-database"] = $e->getMessage();
        }

        try {
            DB::statement("DROP USER IF EXISTS '$user->database_username'@'localhost';");
            DB::statement("DROP USER IF EXISTS '$user->database_username'@'%';");
        } catch (Exception $e) {
            $errors["exception-drop-user"] = $e->getMessage();
        }

        try {
            DB::statement("FLUSH PRIVILEGES;");
        } catch (Exception $e) {
            $errors["exception-flush-privileges"] = $e->getMessage();
        }

        $user->delete();

        if(empty($errors)) {
            Session::flash('message', 'Successfully deleted user!');

            return Redirect::to(action('Admin\UsersController@index'));
        } else {
            $errorMessage = array();
            $errorMessage['exception'] = "User $user->display_name was deleted, but the following exceptions happened:";
            $errors = array_merge($errorMessage, $errors);

            return Redirect::to(action('Admin\UsersController@index'))->
                        withErrors($errors)->
                        withInput();
        }
    }
}
