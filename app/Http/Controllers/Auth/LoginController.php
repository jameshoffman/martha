<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    public function username() {
        return 'username';
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function showLoginForm() {
        if (Auth::check()) {
            return redirect('/');
        } else {
            return view('auth.login');
        }
    }

    public function login(Request $request) {
        $input = $request->all();

        $username = $input['username'];
        $password = $input['password'];
        $remember = array_key_exists('remember', $input);

        if (Auth::attempt(['username' => $username, 'password' => $password], $remember)) {
            return redirect()->intended('/');
        }

        return redirect(action('Auth\LoginController@showLoginForm'))->withInput($request->only('username'))->withErrors(['credentials' => 'Invalid credentials!']);
    }

    public function logout() {
        Auth::logout();

        return redirect('/');
    }
}
