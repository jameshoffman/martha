<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Queries\QueriesController;
use App\Model\Query;
use App\Services\QueriesService;
use App\Services\TablesService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;

class TablesController extends Controller {

    public function index(Request $request) {
        $user = Auth::user();

        $search = trim($request["search"]);

        $tables = $user->getTables($search);

        return View::make('tables.index', compact(['tables', 'search']));
    }

    public function mysqlDump() {
        return TablesService::mySqlDumpForUser(Auth::user(), action('TablesController@index'));
    }

    public function importMySqlDump() {
        return TablesService::importMySqlDumpForUser('mysqldump_file',
                                                     Auth::user(),
                                                     action('TablesController@index'));
    }
}