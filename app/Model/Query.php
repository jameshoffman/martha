<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Validation\Rule;

class Query extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name',
        'string',
        'type'
    ];

    public static function validationRules($userId, $id = 0) {
        return array(
            'query' => 'required',
            'name' => [ 'required', 'max:191',
                        'unique:queries,id,',
                        Rule::unique('queries')->ignore($id)->where(function ($query) use ($userId) {$query->where('user_id', $userId);})],
            'type' => 'required'
        );
    }

    public function user() {
        return $this->belongsTo('App\Model\User');
    }

    public function getTypeNameAttribute() {
        return $this->type == 0 ? 'select' : 'statement';
    }

    public function setTypeNameAttribute($value) {
        $this->attributes['type'] = $value == 'select' ? 0 : 1;
    }

    public function setNameAttribute($value) {
        $this->attributes['name'] = str_replace(" ", "-", trim($value));
    }

    public function parameterized(array $params) {
        $parameterizedString = $this->string;

        foreach ($params as $index => $param) {
            if (is_array($param)) {
                $parameterizedString = $this->parameterized($param);
            } else {
                $parameterizedString = preg_replace(
                    "/\?$index\b/",
                    ($param ?? "null"), // Transform JSON null into "null" to correctly inject in SQL
                    $parameterizedString
                );
            }
        }

        return $parameterizedString;
    }
}
