<?php

namespace App\Model\Responses;

class ImportQueriesResponse
{
    public $totalQueryInserted = 0;
    public $duplicatesQueries = [];
    public $errors = array();

    public function __construct($totalQueryInserted, $duplicatesQueries, $errors) {
        $this->totalQueryInserted = $totalQueryInserted;
        $this->duplicatesQueries = $duplicatesQueries;
        $this->errors = $errors;
    }
}