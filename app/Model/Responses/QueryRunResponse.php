<?php

namespace App\Model\Responses;

class QueryRunResponse {
    public $result = array();
    public $parameterizedQuery = '';
    public $parameters = null; // is a string so it contains exactly what was received in the request

    /**
     * QueryRunResponse constructor.
     * @param array $result
     * @param string $parameterizedQuery
     * @param array $parameters
     */
    public function __construct(array $result, $parameterizedQuery, $parameters)
    {
        $this->result = $result;
        $this->parameterizedQuery = $parameterizedQuery;
        $this->parameters = $parameters;
    }
}