<?php

namespace App\Model;

use App\Http\Controllers\Queries\QueriesController;
use App\Services\UIHelperService;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class User extends Authenticatable
{
    use Notifiable;

    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'display_name', 'username', 'password',
    ];

    protected $casts = [
        'is_admin' => 'boolean',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'is_admin'
    ];

    public function setPasswordAttribute($pwd) {
        $this->attributes['password'] = bcrypt($pwd);
    }

    public function queries() {
        return $this->hasMany('App\Model\Query');
    }

    public function apiRequests() {
        return $this->hasMany('App\Model\ApiRequest');
    }

    public function queryNamed($name) {
        return $this->queries()->where('name', $name)->first();
    }

    public function getDatabaseConnectionAttribute() {
        Config::set('database.connections.tenant_mysql.database', $this->database_name);
        Config::set('database.connections.tenant_mysql.username', $this->database_username);
        Config::set('database.connections.tenant_mysql.password', $this->database_password);
        DB::reconnect('tenant_mysql');

        return DB::connection('tenant_mysql');
    }

    public function getTables($searchCriteria = ''){
        $connection = $this->databaseConnection;
        $tables = array();

        $tablesInClause = 'Tables_in_' . $this->database_name;
        $databaseTables = $connection->select('show full tables where Table_Type = "BASE TABLE" and `' . $tablesInClause . '` like "%' . $searchCriteria .'%"');

        $tablesName = array_map(function($value) use ($tablesInClause) {
            return $value->$tablesInClause;
        }, $databaseTables);

        asort($tablesName);

        foreach ($tablesName as $tableName) {
            $table = array();

            $table['name'] = $tableName;

            $columns = $connection->select('describe ' . $tableName);
            $table['structure']['columns'] = UIHelperService::dataArrayToTable($columns);

            $constraints = $connection->select('select COLUMN_NAME as \'Column\', CONSTRAINT_NAME as \'Name\', CONCAT(REFERENCED_COLUMN_NAME, \'.\', REFERENCED_TABLE_NAME) as \'References\' from information_schema.KEY_COLUMN_USAGE where TABLE_NAME = \'' . $tableName . '\'');
            $table['structure']['constraints'] = UIHelperService::dataArrayToTable($constraints);

            $data = $connection->select('select * from ' . $tableName);
            $table['data'] = UIHelperService::dataArrayToTable($data);

            $createTable = $connection->select('show create table ' . $tableName)[0]->{'Create Table'};
            $table['create_table'] = $createTable;

            array_push($tables, $table);
        }

        return $tables;
    }

    public function getFilteredQueries($criteria = null) {
        $queries = $this->queries();

        if ($criteria) {
            $searchParams = explode(" ", trim($criteria));

            foreach ($searchParams as $param) {
                $searchCriteria = ('%' . $param . '%');

                $queries = $queries->where(function ($query) use ($searchCriteria) {
                    $query->where('name', 'like', $searchCriteria)
                          ->orWhere('string', 'like', $searchCriteria);

                });
            }
        }

        return $queries->orderBy('name', 'ASC')->get();
    }

    public function getFilteredApiRequests($criteria = null) {
        $apiRequests = $this->apiRequests();

        if ($criteria) {
            $search = trim($criteria);

            $searchCriteria = ('%' . $search . '%');
            $apiRequests = $apiRequests->where(function ($query) use ($searchCriteria) {
                $query->where('body_parameters', 'like', $searchCriteria)
                      ->orWhere('query_name', 'like', $searchCriteria)
                      ->orWhere('query_string', 'like', $searchCriteria)
                      ->orWhere('query_result', 'like', $searchCriteria);

            });
        }

        return $apiRequests->latest()->get();
    }
}
