<?php

namespace App\Model;

use App\Observers\ApiRequestObserver;
use Illuminate\Database\Eloquent\Model;

class ApiRequest extends Model
{
    public static function boot() {
        parent::boot();

        ApiRequest::observe(new ApiRequestObserver());
    }

    public function setUpdatedAt($value) {/* Do nothing because only created_at is used */ }

    public function user() {
        return $this->belongsTo('App\Model\User');
    }
}
