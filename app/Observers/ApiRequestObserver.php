<?php

namespace App\Observers;

use App\Model\ApiRequest;
use Illuminate\Support\Facades\DB;


class ApiRequestObserver {
    public function saved(ApiRequest $apiRequest)
    {
        $userId = $apiRequest->user_id;
        $maxRecords = env('MARTHA_MAX_API_REQUESTS_RECORD_PER_USER', 10);

        DB::statement("delete from api_requests 
                       where user_id = $userId 
                       AND id < (SELECT MIN(t.id) from (
                          select sub_t.id as id from api_requests sub_t
			  		      where sub_t.user_id = $userId
			  		      order by sub_t.created_at DESC
			  		      limit $maxRecords) t
			              );
                      ");
    }
}