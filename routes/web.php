<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/login', 'Auth\LoginController@login');
Route::get('/logout', 'Auth\LoginController@logout');

Route::group(['middleware' => 'auth'], function () {
    Route::get('/', function () {
        if (Auth::user()->is_admin) {
            return redirect(action('Admin\UsersController@index'));
        } else {
            return redirect(action('Queries\QueryBuilderController@index'));
        }
    });

    Route::get('/about', 'AboutController@index');
    Route::get('/about/changelog', 'AboutController@changelog');

    Route::get('/builder', 'Queries\QueryBuilderController@index');
    Route::post('/builder/execute', 'Queries\QueryBuilderController@execute');

    Route::get('/queries', 'Queries\QueriesController@index');
    Route::post('/queries', 'Queries\QueriesController@store');
    Route::post('/queries/import', 'Queries\QueriesController@import');
    Route::post('/queries/export', 'Queries\QueriesController@export');

    Route::get('/queries/{id}', 'Queries\QueriesController@show');
    Route::delete('/queries/{id}', 'Queries\QueriesController@destroy');
    Route::put('/queries/{id}', 'Queries\QueriesController@update');

    Route::get('/tables', 'TablesController@index');
    Route::post('/tables/mysql-dump', 'TablesController@mySqlDump');
    Route::post('/tables/mysql-dump/import', 'TablesController@importMySqlDump');

    Route::get('/api-requests', 'ApiRequestsController@index');
    Route::delete('/api-requests', 'ApiRequestsController@purge');
});

Route::group(['middleware' => ['auth', 'auth.admin']], function () {
    Route::get('/users', 'Admin\UsersController@index');
    Route::post('/users', 'Admin\UsersController@store');

    //User details
    Route::get('/users/{id}', 'Admin\UsersController@show');
    Route::patch('/users/{id}', 'Admin\UsersController@update');
    Route::delete('/users/{id}', 'Admin\UsersController@destroy');

    Route::post('/users/{id}/execute-query', 'Admin\UserDetailsController@executeQuery');

    Route::post('/users/{id}/import-queries', 'Admin\UserDetailsController@importQueries');
    Route::post('/users/{id}/export-queries', 'Admin\UserDetailsController@exportQueries');

    Route::post('/users/{id}/mysql-dump', 'Admin\UserDetailsController@mySqlDump');
    Route::post('/users/{id}/mysql-dump/import', 'Admin\UserDetailsController@importMySqlDump');

    Route::delete('/users/{id}/api-requests', 'Admin\UserDetailsController@purgeApiRequests');
});

/*
 * API
 */
 
 

Route::group(['middleware' => ['cors.query-execute']], function () {
    Route::options('/queries/{name}/execute', function ($page) {
        return 'options preflight';
    });
    Route::post('/queries/{name}/execute', 'Queries\QueriesController@execute');
});


/*
 * catch-all
 */

Route::any( '{catchall}', function ($page) {
    return redirect('/');
})->where('catchall', '(.*)');

